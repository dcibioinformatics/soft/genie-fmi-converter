import os.path

from lxml import etree
import re


rr = "{http://integration.foundationmedicine.com/reporting}"
vr = "{http://foundationmedicine.com/compbio/variant-report-external}"
schema = "{http://www.w3.org/2001/XMLSchema}"
instance = "{http://www.w3.org/2001/XMLSchema-instance}"

matches = [(val, re.compile(r"^{}".format(val))) for val in (rr, vr, schema, instance)]


class EmptyNode(object):
    def __init__(self):
        self.text = ""
        self.attrib = {}
        self.tag = ""


def output_cnvs(iid, cnvs, genes):
    """
    Function responsible for create the file data_CNA....
    :param iid: (str) institution name
    :param cnvs: (list) python list of dictionaries containing {deidentification: {
    'mutation':amplification, 'mutation2': amplification2
    }}
    :return: results written to file data_CNA_{iid}.txt
    """
    outfile = "data_CNA_{}.txt".format(iid)
    # keys = [list(x.keys())[0] for x in cnvs]
    keys = []
    unique_genes = set()
    for i in cnvs:
        unique_genes.update(list(list(i.values())[0].keys()))
    complete_cnvs = []
    user_genes = []
    if len(genes)>0:
        if os.path.exists(genes):
            with open(genes) as gene_file:
                glines = gene_file.readlines()
                for gline in glines:
                    user_genes.append(gline.strip())
        else:
            Exception('A -g, --genes switch was provided, but no file exists in that location. Please confirm absolute path and submit again')
    unique_genes.update(user_genes)
    for xmlfile in cnvs:
        new_gene_amplifications = {}
        for patsampid, genes_amplifications in xmlfile.items():
            keys.append(patsampid)
            for gene in unique_genes:
                if gene in genes_amplifications.keys():
                    new_gene_amplifications[gene] = genes_amplifications[gene]
                else:
                    new_gene_amplifications[gene] = 'NA'
        complete_cnvs.append(new_gene_amplifications)
        complete_cnvs_dict = {key: [i[key] for i in complete_cnvs] for key in complete_cnvs[0]}
    if len(keys) == len(complete_cnvs):
        with open(outfile, 'w') as out:
            print("Hugo_Symbol", *keys, file=out, sep="\t")
            for gene, values in complete_cnvs_dict.items():
                print(gene,
                      "\t".join(values),
                      file=out, sep="\t")
    else:
        Exception('Trouble with function output_cnvs. Individuals and gene entry lengths do not equal.compl')


def check_xml(xml, schema_file):
    """
    Takes a lxml.etree._ElementTree object and checks against a xsd file.
    :param xml: (lxml.etree._ElementTree) object
    :param schema_file: (str) path to xsd file for checking
    :return: (bool) whether the schema(xml)
    """
    schemaobj = etree.XMLSchema(etree.parse(schema_file))
    return schemaobj(xml)


def suffix(input_matches, inp):
    """
    strips the URL suffix
    :param input_matches:
    :param inp:
    :return: (tuple)
    """
    for m in input_matches:
        if re.match(m[1], inp):
            return m[0], re.sub(m[1], "", inp)

    return "", inp


class Element:
    def __init__(self, matches_txt, node):
        (self.prefix, self.name) = suffix(matches_txt, node.tag)

        self.attrib = node.attrib

        self.text = ""
        if node.text:
            self.text = node.text.strip()

        self.children = []

    def attr(self, inp):
        return self.attrib.get(inp).strip() if self.attrib.get(inp) else None

    def find(self, element_name):
        ret = []
        self.find_path(element_name, ret)
        return ret

    def find_path(self, element_name, pre):
        if self.name == element_name:
            pre.append(self)

        elif self.children:
            for child in self.children:
                child.find_path(element_name, pre)

    # returns first elements matching full path
    def get(self, item, *items):
        if items and item == self.name:
            return self.get(*items)

        for entry in self.children:
            if entry.name == item:
                if items:
                    return entry.get(*items)
                else:
                    return entry
        return Element([], EmptyNode())

    def __str__(self):
        return self.__to_str__(0)

    def __to_str__(self, depth):
        ret = "\n" + "  " * depth + self.name

        if self.text:
            ret += ": '{}'".format(self.text)

        if self.attrib:
            ret += " {{{} }}".format(
                ", ".join("{}: '{}' ".format(key[1], val)
                          for key, val in self.attrib.items()))

        if self.children:
            for child in self.children:
                ret += "  " * (depth + 1) + child.__to_str__(depth + 1)

            ret += "\n"

        return ret


def decompose(node):
    ele = Element(matches, node)
    ele.children = [decompose(child) for child in node]
    return ele


def parse_cnv(paths, iid, p_ident, s_ident):
    """
    Here we're keeping a dictionary of gene : {sample : copy-number}
    :param paths: (list)
    :param iid: (str)
    :param p_ident: (str)
    :param s_ident: (str)
    :return: (dict), {sample name: {gene: amplification #}}
    """
    sample = "GENIE-{}-{}-{}".format(iid, p_ident, s_ident)
    values = {path.attr("gene"): str(reg(path.attr("type"), path.attr("equivocal"))) for path in paths}
    return {sample: values}


def reg(type, equiv):
    """
    Function that contains logic to determine the values written to the CNA file.
    :param type: (str) type attribute extracted from the xml file
    :param equiv: (str) equivalence extracted from the xml file
    :return:
    """
    if equiv.lower() == 'true':
        equi = True
    elif equiv.lower() == 'false':
        equi = False
    if type not in ('amplification', 'loss'):
        Exception('In the copy number attribute, the type is not loss or amplification. Check the xml file.')

    if isinstance(equi, bool) & isinstance(type, str):
        if equi and type == 'amplification':
            return 2
        elif equi and type == 'loss':
            Exception('Equivocal is True and type is loss, technically this is -1, but FM does not report single-copy loss')
        elif not equi and type == 'amplification':
            return 2
        elif not equi and type == 'loss':
            return -2
        else:
            Exception('Error in parsing copy-number xml paths. Trouble with equivocal or copy-number')
    else:
        Exception('The copy-number parsing has failed. Equivocal is not equal to "true" or "false"')
