class VCFEntry:

    def __init__(self, simplevcfreaderobj, remove_prefix):
        """
        Init method for VCFEntry Class. Can assume strand is positive based on feedback
        :param simplevcfreaderobj: (dict) each key pertains to the column in VCF file and values are values.
        :param remove_prefix: (str) chromosome prefix to remove supplied as argument to command line
        """
        self.seq = simplevcfreaderobj['REF']
        self.altseq = simplevcfreaderobj['ALT']
        self.length = 0
        self.chr = simplevcfreaderobj['CHROM'].replace(remove_prefix, '')
        self.start = simplevcfreaderobj['POS']
        self.info = simplevcfreaderobj['INFO']
        self.end = None
        self.info_parse()
        self.maf_start = None
        self.maf_ref = None
        self.maf_alt = None
        self.maf_altcount = None
        self.maf_depth = None
        self.maf_rules()
        # self.read(filename, text, start)

    def __str__(self):
        return "(" + " ".join((str(x) for x in (
            self.chr, self.start, self.seq,
            self.altseq))) + ")"

    def info_parse(self):
        """
        VCF file comes with variable INFO section; here we parse and assign values to self.
        :return: Assing self attributes
        """
        info = self.info
        d = dict(x.split("=") for x in info.split(";"))
        for k, v in d.items():
            setattr(self, k, v)

    def maf_rules(self):
        """
        Rules provided via Foundation Medicine
        :return:
        """
        # Simple Insertions
        if (len(self.seq) == 1) & (len(self.altseq) > 1) & (self.seq == self.altseq[0]):
            self.maf_start = self.start
            self.maf_ref = '-'
            self.maf_alt = self.altseq[1:]
        # Simple Deletions
        elif (len(self.altseq) == 1) & (len(self.seq) > 1) & (self.altseq == self.seq[0]):
            self.maf_start = str(int(self.start)+1)
            self.maf_ref = self.seq[1:]
            self.maf_alt = '-'
        # All Other Cases; SNV, MNV, delins
        else:
            self.maf_start = self.start
            self.maf_ref = self.seq
            self.maf_alt = self.altseq
        self.maf_depth = self.depth
        self.maf_altcount = str(round(int(self.depth) * float(self.af)))
