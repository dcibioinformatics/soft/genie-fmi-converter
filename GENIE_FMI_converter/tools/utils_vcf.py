from GENIE_FMI_converter.tools.vcfentry import VCFEntry


def simplevcfgenerator(filename):
    """
    Creates generator from a vcf file
    :param filename: str: name of file on system
    :return: generator
    """
    with open(filename, "rt") as ifile:
        data = []
        for line in ifile:
            if line.startswith("#CHROM"):
                vcf_names = [x.rstrip().replace('#', '') for x in line.split('\t')]
            elif line.startswith('#'):
                continue
            else:
                data.append([x.rstrip() for x in line.split('\t')])
        lengthsdata = [len(x) for x in data]
        if len(set(lengthsdata)) == 1:
            myiterable = [
                dict(zip(vcf_names, datum))
                if (len(vcf_names) == len(datum)) else Exception('VCF parser error.') for datum in data
            ]
    for element in myiterable:
        yield element


def generate_maf_via_vcf(vcffilereader, iid, prefix, p_ident, s_ident, out):
    """
    Function responsible for gathering data from vcf and generating maf output
    :param vcffilereader: (str) vcf file name
    :param iid: (str) name of institution (e.g. Duke)
    :param prefix: (str) chromosome prefix from xml (e.g. 'chr')
    :param p_ident: (str) p_ident from manifest
    :param s_ident: (str) s_ident from manifest
    :param out: (_io.TextIOWrapper) pointer to the maf file (i.e. data_mutations_extended_{iID}.txt)
    :return: (str) tab-delimited text that prints to the maf file. Errors print to one of the other two file pointers.
    """
    sample = "GENIE-{}-{}-{}".format(iid, p_ident, s_ident)

    for x in vcffilereader:
        vcfentry = VCFEntry(x, remove_prefix=prefix)
        if not vcfentry.seq or not vcfentry.altseq:
            Exception('No sequence parsed (reference/alt) for vcfentry.')
        print("{}\t{}".format(
            vcfentry.chr,
            vcfentry.maf_start
        ), vcfentry.maf_ref, vcfentry.maf_alt, sample, vcfentry.maf_altcount, vcfentry.maf_depth, file=out, sep="\t")
