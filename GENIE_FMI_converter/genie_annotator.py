import argparse as ap
import csv
import os
from GENIE_FMI_converter.tools.utils_vcf import simplevcfgenerator, generate_maf_via_vcf
from GENIE_FMI_converter.tools.utils_xml import check_xml, decompose, parse_cnv, output_cnvs
from lxml import etree
from pkg_resources import resource_filename
import pathlib
from GENIE_FMI_converter.tools.termcolors import BColors

resource_path = resource_filename('GENIE_FMI_converter', '_resources')
# force POSIX-style path, even on Windows
resource_path = pathlib.Path(resource_path).as_posix()
schema = "/".join([resource_path, "ResultsReport.2.1.xsd"])


def process_one_line_manifest(vcffilename, xmlfilename, iid, prefix, p_ident, s_ident, mafout):
    """
    For each line listed in the manifest, process data to create MAF and CNV output.
    :param vcffilename: (str) vcf filename
    :param xmlfilename: (str) xml filename
    :param iid: (str) institution name
    :param prefix: (str) how chromosomes are listed in xml file (e.g. 'chr')
    :param p_ident: (str) p_ident
    :param s_ident: (str) s_ident
    :param mafout: (_io.TextIOWrapper) pointer to the maf output file
    :return: Generates output for maf, return dictionary for aggregate CNV processing
    """
    # output MAF
    vcf_reader = simplevcfgenerator(vcffilename)
    generate_maf_via_vcf(vcf_reader, iid, prefix, p_ident, s_ident, mafout)

    xml = etree.parse(xmlfilename)
    # validate against schema
    if check_xml(xml, schema):
        # parse XML into object
        obj = decompose(xml.getroot())
        paths = obj.find("copy-number-alteration")
        return parse_cnv(paths, iid, p_ident, s_ident)
    else:
        Exception("File does not validate: {}".format(xmlfilename))


def process(filename, iid, prefix, genes):
    """
    The main function for this python command line tool. Responsible for calling all necessary functions.
    :param filename: (str) csv-delimited file, 4 columns (xml path, vcf path, p_ident, s_ident)
    :param iid: (str) name of institution
    :param prefix: prefix for how chromosomes are listed in xml file
    :return: 2 files: data_mutations_extended_{iid}.txt data_CNA_{iid}.txt
    """
    # write MAF header
    with open("data_mutations_extended_{}.txt".format(iid), 'w') as mafout:
        print(
            "Chromosome", "Start_Position", "Reference_Allele", "Tumor_Seq_Allele2", "Tumor_Sample_Barcode",
            "t_alt_count", "t_depth", file=mafout, sep="\t"
        )

        cnvs = []

        with open(filename, 'r') as inf:
            reader = csv.DictReader(inf, fieldnames=['xml', 'vcf', 'pat', 'samp'])

            for line in reader:
                try:
                    os.path.exists(line['xml'])
                except FileNotFoundError:
                    print('The xml file path provided (Column 1) in the manifest cannot be found.')
                try:
                    os.path.exists(line['vcf'])
                except FileNotFoundError:
                    print('The vcf file path provided (Column 2) in the manifest cannot be found.')
                cnv_output = process_one_line_manifest(
                    vcffilename=line['vcf'],
                    xmlfilename=line['xml'],
                    iid=iid,
                    prefix=prefix,
                    p_ident=line['pat'],
                    s_ident=line['samp'],
                    mafout=mafout
                )
                cnvs.append(cnv_output)
    output_cnvs(iid, cnvs, genes)


def main():
    args = vars(parser.parse_args())

    process(
        filename=args["file"],
        iid=args["i,__institution"],
        prefix=args["p,__prefix"],
        genes=args["g,__genes"]
    )

    print(
        f"{BColors.OKCYAN}Success! " +
        f"{BColors.HEADER}Your MAF file, " +
        f"{BColors.OKBLUE}data_mutations_extended_"+args["i,__institution"]+'.txt, ' +
        f"{BColors.HEADER} and your CNA file, " +
        f"{BColors.OKGREEN}data_CNA_" + args["i,__institution"] + '.txt, ' +
        f"{BColors.HEADER} are in the current directory and ready for verification.{BColors.ENDC}"
    )

parser = ap.ArgumentParser()
parser.add_argument("file", type=str, help="Manifest input file (*.csv) containing no headers and no quotes. Columns "
                                           "should be ordered as xml path, vcf path, "
                                           "patient (de-identified) identifier, sample (de-identified) identifier")
parser.add_argument("-p,--prefix", type=str, help="prefix for chromosome lookup", default="chr")
parser.add_argument("-i,--institution", type=str, help="institution ID", default="Duke")
parser.add_argument("-g,--genes", type=str, help="A text file that includes list of genes to include in CNA output", default="")


if __name__ == '__main__':
    main()
