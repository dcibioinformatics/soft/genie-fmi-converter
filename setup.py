"""
Setup script for the GenieAnnotator package
"""
from setuptools import setup, find_packages

# read in version string
VERSION_FILE = 'GENIE_FMI_converter/_version.py'
__version__ = None  # to avoid inspection warning and check if __version__ was loaded
exec(open(VERSION_FILE).read())

if __version__ is None:
    raise RuntimeError("__version__ string not found in file %s" % VERSION_FILE)

with open("README.md", "r", encoding='utf-8') as fh:
    long_description = fh.read()

reqs = [
    'lxml',
]

setup(
    name='GENIE_FMI_converter',
    version=__version__,
    packages=find_packages(),
    # packages=find_packages(exclude=["GENIE_FMI_converter/tests/"]),
    entry_points={
        'console_scripts': ['GENIE_FMI_converter=GENIE_FMI_converter.genie_annotator:main']
    },
    package_data={'': ['_resources/*.xsd']},
    include_package_data=True,
    description='GENIE MAF Annotator',
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="DCI Bioinformatics Shared Resource",
    license='GNU GENERAL PUBLIC LICENSE Version 2',
    url="https://gitlab.oit.duke.edu/dcibioinformatics/soft/genie-fmi-converter/",
    ext_modules=[],
    install_requires=reqs,
    classifiers=[
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.7'
    ]
)