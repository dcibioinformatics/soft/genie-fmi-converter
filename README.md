# GENIE-FMI-converter

The GENIE-FMI-converter tool has been developed by the Duke Cancer
Institute (DCI) for the Genomics Evidence Neoplasia Information
Exchange (GENIE) project to facilitate conversion of Foundation
Medicine (FMI) reports provided as Variant Call Format (VCF;
(https://samtools.github.io/hts-specs/VCFv4.1.pdf) and XML (Extensible
Markup Language; https://www.w3.org/standards/xml/core) files to the
Mutation Annotation Format (MAF;
https://docs.gdc.cancer.gov/Encyclopedia/pages/Mutation_Annotation_Format/MAF).


## Disclaimer

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

**DO NOT RELEASE THIS UNTIL TEST SUITE HAS BEEN IMPLEMENTED.**


## Requirements

The use of the converter requires Python 3 (https://www.python.org/)
and the lxml (https://lxml.de/) module. It has been tested using
Python 3.9.12 and lxml 4.8.0.


## Installation

Currently, the package is hosted on the Duke University Office of Information
technology (OIT) gitlab repository. Assuming that git and python are installed,
the converter can be installed as follows:


```
# Clone the repository
$ git clone https://gitlab.oit.duke.edu/dcibioinformatics-internal/soft/genie-fmi-converter.git

# Change into the project directory

$ cd genie-fmi-converter

# Install tool

$ pip install \
	--trusted-host pypi.org \
	--trusted-host pypi.python.org \
	--trusted-host files.pythonhosted.org .
```

**Please see the document [RELEASE_NOTES.md](RELEASE_NOTES.md) for
modification to code over time.**

## Current 

A previous version of this tool retrieved reference sequence
information from a user supplied FASTA file.  The new release
retrieves reference sequence and genomic annotation exclusively from
FMI VCF files.

The use of the tool requires the provision of a a comma-separated
values (csv) text file consisting of four columns: path to FMI xml
file, path to FMI vcf file, a patient de-identification label and a
sample de-identification label. The following is a toy example
(note that there are two samples, S1 and S2,from patient P1, and
one sample, S1, from patient P2):

![](images/new_manifest.png)  


The gene list file is a simple text file that contains the names of
genes to include in the data_CNA_xxx.txt file. For
example:  

![](images/famous_genes.png)


An example of how to use the current version of the
tool is provided below (list.csv is the manifest file
and gene.txt is the gene list file):

```
# Get help information
GENIE_FMI_converter --help
GENIE_FMI_converter -i Duke "C:\Users\list.csv"
GENIE_FMI_converter -i Duke --genes "C:\Users\genes.txt" "C:\Users\list.csv"
``` 

## Output  
When this tool is run, two files will automatically be generated:  
- data_CNA_xxx.txt 
- data_mutations_extended_xxx.txt

The string `xxx` will be replaced with the input from the `-i`
parameter (e.g. xxx=Duke in example above). Values in the document
`data_CNA_xxx.txt` are implemented according to the [bioportal
specification](https://docs.cbioportal.org/file-formats/#discrete-copy-number-data).
The logic used to derive these values is as follows:

```
if equi and type == 'amplification':
    return 2
elif equi and type == 'loss':
    Exception('Equivocal is True and type is loss; FM does not report single-copy loss')
elif not equi and type == 'amplification':
    return 2
elif not equi and type == 'loss':
    return -2
else:
    Exception('Error in parsing copy-number xml paths. Trouble with equivocal or copy-number')
```

Notes:

- The values `equi` and `type` are annotations from the input xml file
and correlate to the fields `equivocal` and `type` respectively
- A return code of 2 indicates evidence for the presence of DNA
amplification (3 or more copies) in the gene
- A return code of -2 indicates unequivocal evidence for the presence of DNA
deletion (zero copies; homozygous deletion)  in the gene
- FM does not report single-copy loss (hemizygous deletions). Accordingly,
the code will throw an exception if equi and type == 'loss'



#### Special Notes

- A previous version of this converter required the provision of
an **.xsd** file and a **.fasta** file. These files are no longer
required as an **.xsd** file has been packaged with the current
release and the converter queries reference sequencing information
directly from the FMI VCF file.
- A previous version of this converter used values of -2 in the file
`data_CNA_xxx.txt` when certain genes were not mentioned in the xml file. However,
in this iteration values of `NA` are used in these instances.
