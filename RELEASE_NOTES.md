# Release notes 
This is a list of changes to the GENIE-FMI-converter tool.

## Verion 0.2.0  
 - Updated the API to align with project name and incorporation of vcf files.
 - Updated copy-number logic to output missing values (i.e. NA) when a particular mutation not reported on patient's xml.
 - Added docstrings to functions
 - Packaged code so that it can be installed (along with dependencies) via `pip`
 - Removed output of files `mismatching.txt` and `missing.txt`. These files used to check fasta, no longer used. 
Exceptions thrown where data doesn't meet code logic.
 - Removed xsd input requirement. This is now incorporated as data in the package.
 - Removed fasata file input. This is no longer necessary because vcf file processing is done.
 - TODO: Add switch for comprehensive list of genes/markers. Once included complete a left join to 
add output.
 - A new switch (-g, --genes) is added to ensure that certain genes show up on the data_CNA document. 